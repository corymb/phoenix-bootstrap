defmodule Strappy.Repo do
  use Ecto.Repo,
    otp_app: :strappy,
    adapter: Ecto.Adapters.Postgres
end
